import Iconhome from './home.png';
import IconhomeAktif from './homeAktif.svg';
import Iconpopular from './popular.png';
import IconpopularAktif from './popularAktif.svg';
import Iconprofile from './profile.png';
import IconprofileAktif from './profileAktif.svg';

export {Iconhome, IconhomeAktif, Iconpopular, IconpopularAktif, Iconprofile, IconprofileAktif};