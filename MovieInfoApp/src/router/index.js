import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import {
  Home,
  Profile,
  TopRated,
  TvShow,
  Splash,
  Login,
  LoginScreen,
  Register,
  Details,
} from "../pages";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      activeColor="#2BA5CC"
      inactiveColor="white"
      labelStyle={{ fontSize: 14 }}
      barStyle={{
        backgroundColor: "black",
        opacity: 0.6,
      }}
    >
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: "Home",
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="home" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="TopRated"
        component={TopRated}
        options={{
          tabBarLabel: "Top Rated",
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons
              name="trending-up"
              color={color}
              size={26}
            />
          ),
        }}
      />
      <Tab.Screen
        name="TvShow"
        component={TvShow}
        options={{
          tabBarLabel: "Tv Show",
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="youtube-tv" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarLabel: "Profile",
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="account" color={color} size={26} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};
const Router = () => {
  return (
    <Stack.Navigator initialRouteName="Splash">
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Details"
        component={Details}
      />
    </Stack.Navigator>
  );
};

export default Router;

const styles = StyleSheet.create({});
