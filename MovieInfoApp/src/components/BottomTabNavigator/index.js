import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { BlurView } from "@react-native-community/blur";
import TabItem from "../TabItem";

const BottomTabNavigator = ({ state, descriptors, navigation }) => {
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View style={styles.container}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: "tabPress",
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: "tabLongPress",
            target: route.key,
          });
        };
        <BlurView
          style={styles.absolute}
          blurType="light"
          blurAmount={50}
          reducedTransparencyFallbackColor="white"
        />;

        return (
          <TabItem
            key={index}
            isFocused={isFocused}
            label={label}
            onLongPress={onLongPress}
            onPress={onPress}
          />
        );
      })}
    </View>
  );
};

export default BottomTabNavigator;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    backgroundColor: "black",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 20,
    },
    shadowOpacity: 2,
    shadowRadius: 5,
    elevation: 10,
    opacity: 0.7,
    justifyContent: "space-between",
    paddingHorizontal: 30,
    paddingVertical: 20,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
  },
});
