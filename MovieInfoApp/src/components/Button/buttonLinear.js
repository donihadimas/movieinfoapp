import React from "react";
import { StyleSheet, TouchableOpacity, Text, View } from "react-native";
import { LinearGradient } from "expo-linear-gradient";

export default function FlatButtonLinear({
  text,
  onPress,
  textColor,
  warna1,
  warna2,
  warna3,
}) {
  return (
    <TouchableOpacity onPress={onPress}>
      <LinearGradient colors={[warna1, warna2, warna3]} style={styles.button}>
        <View>
          <Text
            style={{
              color: textColor,
              fontWeight: "bold",
              textTransform: "uppercase",
              textAlign: "center",
              textShadowColor: "#000",
              textShadowRadius: 2,
              textShadowOffset: { width: 0.3, height: 1 },
            }}
          >
            {text}
          </Text>
        </View>
      </LinearGradient>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 20,
    paddingVertical: 14,
    paddingHorizontal: 10,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: { width: 4, height: 10 },
    shadowOpacity: 10,
    shadowRadius: 10,
    elevation: 10,
  },
});
