import React from "react";
import { StyleSheet, TouchableOpacity, Text, View } from "react-native";

export default function FlatButton ({ text, onPress, backgroundColor }) {
  return (
    <TouchableOpacity onPress={onPress} style={[styles.button, backgroundColor && {backgroundColor}]} >
        <View>
            <Text style={styles.buttonText}>{text}</Text>
        </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
    button: {
        borderRadius: 20,
        paddingVertical: 14,
        paddingHorizontal: 10,
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: {width:4, height:10},
        shadowOpacity: 10,
        shadowRadius: 10,
        elevation: 10
    },
    buttonText: {
        color: '#3a5bf6',
        fontWeight: 'bold',
        textTransform: 'uppercase',
        textAlign: 'center',
        textShadowColor: '#000',
        textShadowOffset: {width: 0.3, height:1},
        textShadowRadius: 2,
        
    }
});
