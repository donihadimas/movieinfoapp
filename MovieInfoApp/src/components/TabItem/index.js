import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  Iconhome,
  IconhomeAktif,
  Iconpopular,
  IconpopularAktif,
  Iconprofile,
  IconprofileAktif,
} from "../../assets";

const TabItem = ({ label, isFocused, onLongPress, onPress }) => {
    const Icon = () => {
        if (label === "Home") {
          return (<Iconhome />);
        }
    
        if (label === "TopRated") {
          return (<Iconpopular />);
        }
    
        if (label === "Profile") {
          return (<Iconprofile />);
        }
      };
  return (
    <TouchableOpacity
      onPress={onPress}
      onLongPress={onLongPress}
      style={styles.container}
    >
      <Icon />
      <Text style={isFocused ? styles.textAktif : styles.text}>{label}</Text>
    </TouchableOpacity>
  );
};

export default TabItem;

const styles = StyleSheet.create({
  container: {
    alignContent: "center",
    padding: 5,
  },
  textAktif: {
    color: "#2BA5CC",
    shadowColor: "white",
    shadowOffset: {
      width: 1,
      height: 10,
    },
    shadowRadius: 5,
    shadowOpacity: 5,
    elevation: 5,
  },
  text: {
    color: "white",
    shadowColor: "white",
    shadowOffset: {
      width: 1,
      height: 10,
    },
    shadowRadius: 5,
    shadowOpacity: 5,
    elevation: 5,
  },
});
