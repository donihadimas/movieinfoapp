import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ImageBackground,
  TextInput,
} from "react-native";
import { Background, Logo } from "../../assets";
import FlatButtonLinear from "../../components/Button/buttonLinear";
import { TouchableOpacity } from "react-native-gesture-handler";
import Ionicons from "react-native-vector-icons/Ionicons";

const Register = ({navigation}) => {
  return (
    <ImageBackground source={Background} style={styles.background}>
      <TouchableOpacity onPress={()=> (navigation.goBack())}>
        <Ionicons style={styles.icon} name="ios-arrow-dropleft" size={35} />
      </TouchableOpacity>
      <View style={styles.header}>
        <Text style={styles.judul}>Create an account.</Text>
      </View>
      <View style={styles.desc}>
        <Text style={styles.text} numberOfLines={2}>
          Create new account
        </Text>
      </View>
      <View style={styles.body}>
        <TextInput
          style={styles.input}
          placeholder="Username"
          placeholderTextColor="white"
        />
      </View>
      <View style={styles.body1}>
        <TextInput
          style={styles.input}
          placeholder="Email"
          placeholderTextColor="white"
        />
      </View>
      <View style={styles.body1}>
        <TextInput
          style={styles.input}
          placeholder="Phone"
          placeholderTextColor="white"
          keyboardType="numeric"
        />
      </View>
      <View style={styles.body1}>
        <TextInput
          style={styles.input}
          placeholder="Date of birth"
          placeholderTextColor="white"
          keyboardType="numeric"
        />
      </View>
      <View style={styles.body1}>
        <TextInput
          style={styles.input}
          placeholder="Password"
          placeholderTextColor="white"
          secureTextEntry={true}
        />
      </View>
      <View style={styles.tombol1}>
        <FlatButtonLinear
          text="Sign up"
          textColor="white"
          warna1="#3A5BF6"
          warna2="#0AC5C5"
          warna3="#2BA5CC"
          onPress={()=>(navigation.navigate('LoginScreen'))}
        />
      </View>

      <View style={styles.loginor}>
        <TouchableOpacity onPress={()=>(alert('Coming Soon :)'))}>
          <Text style={styles.or} numberOfLines={2}>
            By the clicking Sign up you agree to the following Terms and
            Conditions without reservation
          </Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

export default Register;

const WindowHeight = Dimensions.get("window").height;
const WindowWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  background: {
    flex: 1,
  },
  header: {
    top: WindowHeight * 0.02,
    left: WindowWidth * 0.01,
    justifyContent: "flex-start",
  },
  judul: {
    padding: 20,
    fontFamily: "Roboto",
    fontSize: 40,
    lineHeight: 20,
    fontWeight: "bold",
    textShadowColor: "rgba(44, 43, 43, 0.6)",
    textShadowOffset: { width: 1.5, height: 2 },
    textShadowRadius: 6,
  },
  desc: {
    marginTop: WindowHeight * 0.01,
    marginLeft: WindowWidth * 0.15,
  },
  text: {
    width: WindowWidth * 0.65,
    color: "#212121",
    fontFamily: "Roboto",
    fontSize: 18,
    fontWeight: "bold",
    lineHeight: 23,
    textShadowColor: "rgba(44, 43, 43, 0.6)",
    textShadowOffset: { width: 1, height: 2 },
    textShadowRadius: 6,
  },
  body: {
    marginTop: WindowHeight * 0.04,
    alignSelf: "center",
    width: WindowWidth * 0.8,
    height: WindowHeight * 0.06,
  },
  input: {
    paddingHorizontal: 20,
    flex: 1,
    alignContent: "center",
    backgroundColor: "black",
    opacity: 0.5,
    borderRadius: 20,
    color: "white",
  },
  body1: {
    marginTop: WindowHeight * 0.03,
    alignSelf: "center",
    width: WindowWidth * 0.8,
    height: WindowHeight * 0.06,
  },
  loginor: {
    marginTop: WindowHeight * 0.06,
    alignSelf: "center",
  },
  or: {
    textAlign: "center",
    width: WindowWidth * 0.8,
    fontSize: 14,
    fontFamily: "Roboto",
    color: "#8c8c8c",
  },
  tombol1: {
    marginTop: WindowHeight * 0.1,
    alignSelf: "center",
    height: WindowHeight * 0.07,
    width: WindowWidth * 0.8,
  },
  icon: {
    marginTop: WindowHeight * 0.035,
    marginLeft: WindowWidth * 0.03,
  },
});
