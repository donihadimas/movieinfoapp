import React from "react";
import {
  StyleSheet,
  ImageBackground,
  View,
  Text,
  Dimensions,
} from "react-native";
import { Background, Logo } from "../../assets";
import FlatButton from "../../components/Button/button";
import FlatButtonLinear from "../../components/Button/buttonLinear";

// Icon
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { TouchableOpacity } from "react-native-gesture-handler";

const Login = ({ navigation }) => {
  return (
    <ImageBackground source={Background} style={styles.background}>
      <View style={styles.header}>
        <Text style={styles.judul}>Find your</Text>
        <Text style={styles.judul1}>favorite movie.</Text>
      </View>
      <View style={styles.desc}>
        <Text style={styles.text} numberOfLines={2}>
          with millions of films from all over the world are here
        </Text>
        <Text style={styles.text1} numberOfLines={2}>
          Join us, and we'll make you happy
        </Text>
      </View>

      {/* Tombol */}

      <View style={styles.tombol}>
        <FlatButton
          text="Login"
          backgroundColor="white"
          onPress={() => navigation.navigate("LoginScreen")}
        />
      </View>

      <View style={styles.tombol1}>
        <FlatButtonLinear
          text="Sign up"
          textColor="white"
          warna1="#3A5BF6"
          warna2="#0AC5C5"
          warna3="#2BA5CC"
          onPress={() => navigation.navigate("Register")}
        />
      </View>

      <View style={styles.loginor}>
        <Text style={styles.or}>Or Log in with</Text>
      </View>

      <View style={styles.social}>
        <TouchableOpacity onPress={()=>(alert('Coming Soon :)'))}>
          <MaterialCommunityIcons style={styles.icon} name="google" size={30} />
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>(alert('Coming Soon :)'))}>
          <SimpleLineIcons
            style={styles.icon}
            name="social-facebook"
            size={30}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>(alert('Coming Soon :)'))}>
          <SimpleLineIcons
            style={styles.icon}
            name="social-twitter"
            size={30}
          />
        </TouchableOpacity>
      </View>
      <Text
        style={{
          textAlign: "center",
          color: "#f5f5f5",
          marginTop: WindowHeight * 0.03,
          fontStyle: "italic",
        }}
      >
        Prototype{" "}
        <MaterialCommunityIcons
          name="copyright"
          style={{ alignItems: "center" }}
        />{" "}
        2020
      </Text>
    </ImageBackground>
  );
};

export default Login;

const WindowHeight = Dimensions.get("window").height;
const WindowWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  background: {
    flex: 1,
  },
  header: {
    top: WindowHeight * 0.2,
    left: WindowWidth * 0.05,
    justifyContent: "flex-start",
  },
  judul: {
    padding: 20,
    fontFamily: "Roboto",
    fontSize: 40,
    lineHeight: 20,
    fontWeight: "bold",
    textShadowColor: "rgba(44, 43, 43, 0.6)",
    textShadowOffset: { width: 1.5, height: 2 },
    textShadowRadius: 6,
  },
  judul1: {
    top: -10,
    left: 45,
    padding: 20,
    fontFamily: "Roboto",
    fontSize: 40,
    lineHeight: 20,
    fontWeight: "bold",
    textShadowColor: "rgba(44, 43, 43, 0.6)",
    textShadowOffset: { width: 1.5, height: 2 },
    textShadowRadius: 6,
  },
  desc: {
    marginTop: WindowHeight * 0.19,
    marginLeft: WindowWidth * 0.15,
  },
  text: {
    width: WindowWidth * 0.65,
    color: "#212121",
    fontFamily: "Roboto",
    fontSize: 18,
    fontWeight: "bold",
    lineHeight: 23,
    textShadowColor: "rgba(44, 43, 43, 0.6)",
    textShadowOffset: { width: 1, height: 2 },
    textShadowRadius: 6,
  },
  text1: {
    left: 35,
    width: WindowWidth * 0.65,
    color: "#212121",
    fontFamily: "Roboto",
    fontSize: 18,
    fontWeight: "bold",
    lineHeight: 23,
    textShadowColor: "rgba(44, 43, 43, 0.6)",
    textShadowOffset: { width: 0.2, height: 1 },
    textShadowRadius: 4,
  },
  tombol: {
    marginTop: WindowHeight * 0.15,
    alignSelf: "center",
    height: WindowHeight * 0.07,
    width: WindowWidth * 0.8,
  },
  tombol1: {
    marginTop: WindowHeight * 0.02,
    alignSelf: "center",
    height: WindowHeight * 0.07,
    width: WindowWidth * 0.8,
  },
  loginor: {
    marginTop: WindowHeight * 0.04,
    alignSelf: "center",
  },
  or: {
    fontSize: 14,
    fontFamily: "Roboto",
    color: "#8c8c8c",
  },
  social: {
    marginTop: WindowHeight * 0.04,
    flexDirection: "row",
    justifyContent: "space-evenly",
    paddingHorizontal: 60,
  },
  icon: {
    color: "#f8f8f8",
  },
});
