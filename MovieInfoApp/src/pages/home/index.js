import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ImageBackground,
  FlatList,
} from "react-native";
import { TextInput } from "react-native-paper";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { SafeAreaView } from "react-native-safe-area-context";
import { StatusBar } from "expo-status-bar";
import { BackgroundHome } from "../../assets";

//API

import Var from "../../components/var";
import { TouchableOpacity, ScrollView } from "react-native-gesture-handler";

const Home = () => {
  const [genres, setGenres] = useState([]);
  const [nowPlaying, setnowPlaying] = useState([]);
  const [Upcoming, setUpcoming] = useState([]);
  const [Tvshow, setTvshow] = useState([]);
  useEffect(() => {
    async function getGenres() {
      try {
        let response = await fetch(
          Var.host + "genre/movie/list?api_key=" + Var.api_key_tmdb
        );
        let json = await response.json();
        setGenres(json.genres);
      } catch (error) {
        console.log(Error);
      }
    }
    getGenres();
    // console.log(genres);
    async function getnowPlaying() {
      try {
        let response = await fetch(
          Var.host + "movie/now_playing?api_key=" + Var.api_key_tmdb
        );
        let json = await response.json();
        setnowPlaying(json.results);
      } catch (error) {
        console.log(Error);
      }
    }
    getnowPlaying();

    async function getUpcoming() {
      try {
        let response = await fetch(
          Var.host + "movie/upcoming?api_key=" + Var.api_key_tmdb
        );
        let json = await response.json();
        setUpcoming(json.results);
      } catch (error) {
        console.log(Error);
      }
    }
    getUpcoming();

    async function getTV() {
      try {
        let response = await fetch(
          Var.host + "tv/on_the_air?api_key=" + Var.api_key_tmdb
        );
        let json = await response.json();
        setTvshow(json.results);
      } catch (error) {
        console.log(Error);
      }
    }
    getTV();
  }, []);

  return (
    <ImageBackground source={BackgroundHome} style={styles.background}>
      <SafeAreaView style={styles.container}>
        <StatusBar backgroundColor="transparent" translucent={true} />
        {/* Header */}
        <View style={{ flex: 1 }}>
          <View style={styles.header}>
            <Text style={styles.texthead}>Movie</Text>
            <TextInput
              style={styles.searchbar}
              placeholder="Search"
              placeholderTextColor="white"
            />
          </View>
        </View>

        {/* kategori */}
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={genres}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item, index }) => <Kategori item={item} />}
        />

        {/* Now Playing */}
        <ScrollView>
          <View style={styles.nowplaying}>
            <TouchableOpacity>
              <Text style={styles.now}>Now Playing</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <MaterialIcons
                style={styles.icon}
                name="keyboard-arrow-right"
                size={45}
              />
            </TouchableOpacity>
          </View>

          {/* Poster */}
          <FlatList
            contentContainerStyle={{ paddingHorizontal: 10 }}
            horizontal
            showsHorizontalScrollIndicator={false}
            data={nowPlaying}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item, index }) => <NowPlaying item={item} />}
          />

          {/* Upcoming */}
          <View style={styles.nowplaying}>
            <TouchableOpacity>
              <Text style={styles.now}>Coming Soon</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <MaterialIcons
                style={styles.icon}
                name="keyboard-arrow-right"
                size={45}
              />
            </TouchableOpacity>
          </View>

          <FlatList
            contentContainerStyle={{ paddingHorizontal: 10 }}
            horizontal
            showsHorizontalScrollIndicator={false}
            data={Upcoming}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item, index }) => <UpComing item={item} />}
          />

          {/* TV Show */}
          <View style={styles.nowplaying}>
            <TouchableOpacity>
              <Text style={styles.now}>TV Show</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <MaterialIcons
                style={styles.icon}
                name="keyboard-arrow-right"
                size={45}
              />
            </TouchableOpacity>
          </View>
          <FlatList
              contentContainerStyle={{ paddingHorizontal: 10 }}
              horizontal
              showsHorizontalScrollIndicator={false}
              data={Tvshow}
              keyExtractor={(item) => item.id.toString()}
              renderItem={({ item, index }) => <TVShow item={item} />}
            />
        </ScrollView>
      </SafeAreaView>
    </ImageBackground>
  );
  function Kategori({ item }) {
    return (
      <TouchableOpacity
        style={styles.kategori}
        onPress={() => alert("Comingsoon!")}
      >
        <View>
          <Text style={styles.textkategori}>{item.name}</Text>
        </View>
      </TouchableOpacity>
    );
  }
  function NowPlaying({ item }) {
    return (
      <TouchableOpacity
        style={styles.poster}
        onPress={() => alert("Comingsoon!")}
      >
        <ImageBackground
          source={{ uri: "https://image.tmdb.org/t/p/w500" + item.poster_path }}
          style={styles.imageposter}
        >
          <Text
            style={{
              color: "white",
              padding: 8,
              backgroundColor: "rgba(0,0,0,0.4)",
            }}
            numberOfLines={2}
            ellipsizeMode="tail"
          >
            {item.title}
          </Text>
          <Text
            style={{
              color: "white",
              backgroundColor: "rgba(0,0,0,0.6)",
              alignSelf: "flex-end",
              padding: 5,
              borderRadius: 50,
              position: "absolute",
              top: 1,
            }}
          >
            {item.vote_average}
          </Text>
        </ImageBackground>
      </TouchableOpacity>
    );
  }

  function UpComing({ item }) {
    var bulan = item.release_date.split("-");
    let tahun = (bulan) => {
      switch (bulan) {
        case "01":
          var namabulan;
          namabulan = "Januari";
          break;
        case "02":
          var namabulan;
          namabulan = "Februari";
          break;
        case "03":
          var namabulan;
          namabulan = "Maret";
          break;
        case "04":
          var namabulan;
          namabulan = "April";
          break;
        case "05":
          var namabulan;
          namabulan = "Mei";
          break;
        case "06":
          var namabulan;
          namabulan = "Juni";
          break;
        case "07":
          var namabulan;
          namabulan = "Juli";
          break;
        case "08":
          var namabulan;
          namabulan = "Agustus";
          break;
        case "09":
          var namabulan;
          namabulan = "September";
          break;
        case "10":
          var namabulan;
          namabulan = "Oktober";
          break;
        case "11":
          var namabulan;
          namabulan = "November";
          break;
        case "12":
          var namabulan;
          namabulan = "Desember";
          break;

        default:
          break;
      }
      return namabulan;
    };

    const releasedate = tahun(bulan[1]);

    return (
      <TouchableOpacity
        style={styles.poster}
        onPress={() => alert("Comingsoon!")}
      >
        <ImageBackground
          source={{ uri: "https://image.tmdb.org/t/p/w500" + item.poster_path }}
          style={styles.imageposter}
        >
          <Text
            style={{
              color: "white",
              padding: 8,
              backgroundColor: "rgba(0,0,0,0.4)",
            }}
            numberOfLines={2}
            ellipsizeMode="tail"
          >
            {item.title}
          </Text>
          <Text
            style={{
              color: "white",
              backgroundColor: "rgba(0,0,0,0.6)",
              alignSelf: "flex-end",
              padding: 5,
              borderRadius: 50,
              position: "absolute",
              top: 1,
            }}
          >
            {releasedate + " " + bulan[0]}
          </Text>
        </ImageBackground>
      </TouchableOpacity>
    );
  }

  function TVShow({ item }) {
    return (
      <TouchableOpacity
        style={styles.poster}
        onPress={() => alert("Comingsoon!")}
      >
        <ImageBackground
          source={{ uri: "https://image.tmdb.org/t/p/w500" + item.poster_path }}
          style={styles.imageposter1}
        >
          <Text
            style={{
              color: "white",
              padding: 8,
              backgroundColor: "rgba(0,0,0,0.4)",
            }}
            numberOfLines={2}
            ellipsizeMode="tail"
          >
            {item.name}
          </Text>
          <Text
            style={{
              color: "white",
              backgroundColor: "rgba(0,0,0,0.6)",
              alignSelf: "flex-end",
              padding: 5,
              borderRadius: 50,
              position: "absolute",
              top: 1,
            }}
          >
            {item.vote_average}
          </Text>
        </ImageBackground>
      </TouchableOpacity>
    );
  }
};

export default Home;

const WindowHeight = Dimensions.get("window").height;
const WindowWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  background: {
    flex: 1,
  },
  container: {},
  header: {
    paddingVertical: 20,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
  },
  texthead: {
    fontFamily: "Roboto",
    fontSize: 30,
    fontWeight: "bold",
    color: "#f5f5f5",
    textShadowColor: "#000",
    textShadowOffset: { width: 1, height: 3 },
    textShadowRadius: 6,
  },
  searchbar: {
    backgroundColor: "transparent",
    width: WindowWidth * 0.55,
    height: 30,
    color: "white",
    borderColor: "white",
    borderWidth: 1.5,
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  kategori: {
    backgroundColor: "#373121",
    opacity: 0.8,
    marginTop: WindowHeight * 0.08,
    paddingVertical: 6,
    paddingHorizontal: 10,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 8,
    borderRadius: 20,
    shadowColor: "#000",
    shadowOffset: { width: 2, height: 10 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
    marginBottom: WindowHeight* 0.03
  },
  textkategori: {
    color: "white",
    fontFamily: "Roboto",
    fontStyle: "italic",
    textShadowColor: "#000",
    textShadowOffset: { width: 1, height: 3 },
    textShadowRadius: 6,
  },
  nowplaying: {
    paddingHorizontal: 18,
    paddingBottom: 5,
    flexDirection: "row",
    justifyContent: "space-between",
    marginLeft: WindowWidth * 0.02,
    marginTop: WindowHeight * 0.01,
    borderLeftWidth: 1,
    borderBottomWidth: 3,
    borderBottomLeftRadius: 20,
    borderBottomColor: "#373121",
    shadowColor: "#0000",
    shadowOffset: { width: 2, height: 10 },
    shadowOpacity: 5,
    shadowRadius: 10,
  },
  now: {
    fontFamily: "Roboto",
    fontSize: 30,
    fontWeight: "bold",
    color: "#f5f5f5",
    textShadowColor: "#000",
    textShadowOffset: { width: 1, height: 3 },
    textShadowRadius: 6,
  },
  icon: {
    color: "white",
    textShadowColor: "#000",
    textShadowOffset: { width: 1, height: 3 },
    textShadowRadius: 6,
    elevation: 5,
  },
  poster: {
    paddingLeft: 10,
    shadowColor: "#0000",
    shadowOffset: { width: 2, height: 10 },
    shadowOpacity: 5,
    shadowRadius: 10,
    elevation: 4,
  },
  imageposter: {
    width: 150,
    height: 220,
    resizeMode: "cover",
    marginTop: WindowHeight * 0.02,
    marginLeft: WindowWidth * 0.01,
    justifyContent: "flex-end",
  },
  imageposter1: {
    width: 150,
    height: 220,
    resizeMode: "cover",
    marginTop: WindowHeight * 0.02,
    marginLeft: WindowWidth * 0.01,
    justifyContent: "flex-end",
    marginBottom: 150,
  },
});
