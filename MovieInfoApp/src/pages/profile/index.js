import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ImageBackground,
  Image,
  TouchableOpacity
} from "react-native";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { SafeAreaView } from "react-native-safe-area-context";
import { BackgroundHome, iconprofile } from "../../assets";
import { StatusBar } from "expo-status-bar";
import { color } from "react-native-reanimated";

const Profile = () => {
  return (
    <ImageBackground source={BackgroundHome} style={styles.background}>
      <SafeAreaView style={styles.container}>
        <StatusBar backgroundColor="transparent" translucent={true} />
        {/* Header */}
        <View>
          <View style={styles.header}>
            <Text style={styles.texthead}>Profile</Text>
          </View>
        </View>
        <View style={{ alignSelf: "center" }}>
          <Image source={iconprofile} />
        </View>
        <View style={styles.nama}>
          <Text
            style={{
              color: 'white',
              fontSize: 20,
              fontWeight: "bold",
              textShadowColor: "#000",
              textShadowOffset: { width: 1, height: 1 },
              textShadowRadius: 6,
            }}
          >
            Doni Hadimas Aprilian
          </Text>
        </View>
        <View style={styles.descib}>
          <Text style={styles.textjudul}>About</Text>
          <Text style={styles.textdesc} numberOfLines={4}>
            I am a student currently studying informatics engineering, i am also
            a light vehicle engineering graduate
          </Text>
          <Text style={styles.kontak}>Contact me</Text>
          <View style={styles.social}>
            <TouchableOpacity onPress={() => alert("Coming Soon :)")}>
              <MaterialCommunityIcons
                style={styles.icon}
                name="google"
                size={30}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => alert("Coming Soon :)")}>
              <SimpleLineIcons
                style={styles.icon}
                name="social-facebook"
                size={30}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => alert("Coming Soon :)")}>
              <SimpleLineIcons
                style={styles.icon}
                name="social-twitter"
                size={30}
              />
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    </ImageBackground>
  );
};

export default Profile;

const WindowHeight = Dimensions.get("window").height;
const WindowWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  background: {
    flex: 1,
  },
  container: {},
  header: {
    paddingVertical: 20,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
  },
  texthead: {
    fontFamily: "Roboto",
    fontSize: 30,
    fontWeight: "bold",
    color: "#f5f5f5",
    textShadowColor: "#000",
    textShadowOffset: { width: 1, height: 3 },
    textShadowRadius: 6,
  },
  nama: {
    width: WindowWidth * 0.8,
    alignSelf: "flex-end",
    backgroundColor: "blue",
    padding: 30,
    opacity: 0.8,
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    zIndex: 11,
  },
  textjudul: {
    fontSize: 30,
    fontWeight: "bold",
    color: "white",
  },
  textdesc: {
    fontSize: 18,
    color: "white",
  },
  descib: {
    marginTop: -50,
    borderTopLeftRadius: 30,
    backgroundColor: "black",
    opacity: 0.8,
    width: WindowWidth * 0.9,
    height: WindowHeight * 1,
    paddingTop: 65,
    alignSelf: "flex-end",
    paddingHorizontal: 20,
  },
  kontak: {
    marginTop: 30,
    color: 'white',
    fontSize: 25,
    alignSelf: 'center'
  },
  social: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 20
  },
  icon: {
    color: 'white'
  }

});
