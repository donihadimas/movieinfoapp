import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ImageBackground,
  FlatList,
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { SafeAreaView } from "react-native-safe-area-context";
import { BackgroundHome } from "../../assets";
import { StatusBar } from "expo-status-bar";
import { color } from "react-native-reanimated";

const TvShow = () => {
  return (
    <ImageBackground source={BackgroundHome} style={styles.background}>
      <SafeAreaView style={styles.container}>
        <StatusBar backgroundColor="transparent" translucent={true} />
        {/* Header */}
        <Text style={{fontSize: 30, fontWeight: 'bold', alignSelf: 'center',color:'white', textShadowColor: '#000', textShadowOffset: {width:1, height:2}, textShadowRadius: 6}}>Tv Show</Text>
        <MaterialCommunityIcons
          style={styles.header}
          name="youtube-tv"
          size={200}
        />
        <Text style={{fontSize: 30, fontWeight: 'bold', alignSelf: 'center',color:'white', textShadowColor: '#000', textShadowOffset: {width:1, height:2}, textShadowRadius: 6}}>Comingsoon!</Text>
      </SafeAreaView>
    </ImageBackground>
  );
};

export default TvShow;

const WindowHeight = Dimensions.get("window").height;
const WindowWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  background: {
    flex: 1,
  },
  container: {},
  header: {
    marginTop: WindowHeight * 0.2,
    justifyContent: "center",
    alignSelf: "center",
    color: "white",
    textShadowRadius: 10,
    textShadowColor: "#000",
    textShadowOffset: { height: 4, width: 1 },
  },
  texthead: {
    fontFamily: "Roboto",
    fontSize: 30,
    fontWeight: "bold",
    color: "#f5f5f5",
    textShadowColor: "#000",
    textShadowOffset: { width: 1, height: 3 },
    textShadowRadius: 6,
  },
});
