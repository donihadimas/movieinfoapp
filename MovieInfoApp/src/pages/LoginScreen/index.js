import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ImageBackground,
  TextInput,
} from "react-native";
import { Background, Logo } from "../../assets";
import FlatButton from "../../components/Button/button";
import FlatButtonLinear from "../../components/Button/buttonLinear";
import { TouchableOpacity } from "react-native-gesture-handler";
import Ionicons from "react-native-vector-icons/Ionicons";

const LoginScreen = ({navigation}) => {
  return (
    <ImageBackground source={Background} style={styles.background}>
      <TouchableOpacity onPress={() => (navigation.goBack())} >
        <Ionicons style={styles.icon} name="ios-arrow-dropleft" size={35} />
      </TouchableOpacity>
      <View style={styles.header}>
        <Text style={styles.judul}>Welcome back.</Text>
      </View>
      <View style={styles.desc}>
        <Text style={styles.text} numberOfLines={2}>
          Log in to your account
        </Text>
      </View>
      <View style={styles.body}>
        <TextInput
          style={styles.input}
          placeholder="Email"
          placeholderTextColor="white"
        />
      </View>
      <View style={styles.body1}>
        <TextInput
          style={styles.input}
          placeholder="Password"
          placeholderTextColor="white"
          secureTextEntry={true}
        />
      </View>
      <View style={styles.tombol}>
        <FlatButton text="Login" backgroundColor="white" onPress={() => (navigation.navigate('MainApp'))} />
      </View>

      <View style={styles.loginor}>
        <TouchableOpacity onPress={()=>(alert('Coming Soon :)'))}>
          <Text style={styles.or}>Forgot your password?</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

export default LoginScreen;

const WindowHeight = Dimensions.get("window").height;
const WindowWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  background: {
    flex: 1,
  },
  header: {
    top: WindowHeight * 0.2,
    left: WindowWidth * 0.05,
    justifyContent: "flex-start",
  },
  judul: {
    padding: 20,
    fontFamily: "Roboto",
    fontSize: 40,
    lineHeight: 20,
    fontWeight: "bold",
    textShadowColor: "rgba(44, 43, 43, 0.6)",
    textShadowOffset: { width: 1.5, height: 2 },
    textShadowRadius: 6,
  },
  desc: {
    marginTop: WindowHeight * 0.19,
    marginLeft: WindowWidth * 0.15,
  },
  text: {
    width: WindowWidth * 0.65,
    color: "#212121",
    fontFamily: "Roboto",
    fontSize: 18,
    fontWeight: "bold",
    lineHeight: 23,
    textShadowColor: "rgba(44, 43, 43, 0.6)",
    textShadowOffset: { width: 1, height: 2 },
    textShadowRadius: 6,
  },
  body: {
    marginTop: WindowHeight * 0.11,
    alignSelf: "center",
    width: WindowWidth * 0.8,
    height: WindowHeight * 0.06,
  },
  input: {
    paddingHorizontal: 20,
    flex: 1,
    alignContent: "center",
    backgroundColor: "black",
    opacity: 0.5,
    borderRadius: 20,
    color: "white",
    
  },
  body1: {
    marginTop: WindowHeight * 0.03,
    alignSelf: "center",
    width: WindowWidth * 0.8,
    height: WindowHeight * 0.06,
  },
  tombol: {
    marginTop: WindowHeight * 0.15,
    alignSelf: "center",
    height: WindowHeight * 0.07,
    width: WindowWidth * 0.8,
  },
  loginor: {
    marginTop: WindowHeight * 0.04,
    alignSelf: "center",
  },
  or: {
    fontSize: 14,
    fontFamily: "Roboto",
    color: "#8c8c8c",
  },
  icon: {
    marginTop: WindowHeight * 0.035,
    marginLeft: WindowWidth * 0.03,
  },
});
