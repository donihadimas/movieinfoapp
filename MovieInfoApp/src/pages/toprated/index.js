import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ImageBackground,
  FlatList,
  Image,
} from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { SafeAreaView } from "react-native-safe-area-context";
import { BackgroundHome } from "../../assets";
import { StatusBar } from "expo-status-bar";

import Var from "../../components/var";

const TopRated = () => {
  const [pop, setPop] = useState([]);

  useEffect(() => {
    async function getPop() {
      try {
        let response = await fetch(
          Var.host + "movie/top_rated?api_key=" + Var.api_key_tmdb
        );
        let json = await response.json();
        setPop(json.results);
      } catch (error) {
        console.log(Error);
      }
    }
    getPop();
  }, []);

  return (
    <ImageBackground source={BackgroundHome} style={styles.background}>
      <SafeAreaView style={styles.container}>
        <StatusBar backgroundColor="transparent" translucent={true} />
        {/* Header */}
        <View >
          <View style={styles.header}>
            <Text style={styles.texthead}>Popular Movies</Text>
          </View>
        </View>
      
        <FlatList
        contentContainerStyle={{paddingTop: 20}}
          data={pop}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item, index }) => <TopPop item={item} />}
        />
      </SafeAreaView>
    </ImageBackground>
  );
  function TopPop({ item }) {
    return (
      //  <ImageBackground source={{ uri: "https://image.tmdb.org/t/p/w500" + item.poster_path }} style={styles.imageposter}>

      //  </ImageBackground>
      <View style={styles.flat}>
        <Image
          source={{ uri: "https://image.tmdb.org/t/p/w500" + item.poster_path }}
          style={styles.imageposter}
        ></Image>
        <View style={styles.desc}>
          <Text style={styles.text}>{item.title}</Text>
          <Text style={styles.text1}>{item.release_date}</Text>
          <Text style={styles.text2} numberOfLines={5} ellipsizeMode={'tail'}>{item.overview}</Text>
        </View>
      </View>
    );
  }
};

export default TopRated;

const WindowHeight = Dimensions.get("window").height;
const WindowWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  background: {
    flex: 1,
  },
  container: {},
  header: {
    
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    zIndex: 11
  },
  texthead: {
    fontFamily: "Roboto",
    fontSize: 30,
    fontWeight: "bold",
    color: "#f5f5f5",
    textShadowColor: "#000",
    textShadowOffset: { width: 1, height: 3 },
    textShadowRadius: 6,
    zIndex: 11
  },
  flat:{
    marginTop: WindowHeight*0.04
  },
  card: {
    width: 200,
    height: 400,
    backgroundColor: "white",
  },
  imageposter: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    width: WindowWidth * 0.9,
    height: WindowHeight * 0.4,
    resizeMode: "cover",
    marginTop: WindowHeight * 0.01,
    alignSelf: "center",
  },
  desc: {
    marginTop: -50,
    height: WindowHeight*0.25,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    backgroundColor: 'white',
    width: WindowWidth * 0.9,
    alignSelf: 'center',
    padding: 15
  },
  text:{
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    textShadowColor: "#000",
    textShadowOffset: { width: 0.5, height: 1 },
    textShadowRadius: 6,
 
  },
  text1:{
    fontSize: 16,
    textAlign: 'right',
    textShadowColor: "#000",
    textShadowOffset: { width: 0.5, height: 1 },
    textShadowRadius: 6,
    
  },
  text2:{
    fontSize: 16,
    textAlign: 'left',
    textShadowColor: "#000",
    textShadowOffset: { width: 0.5, height: 0.2 },
    textShadowRadius: 6,
    
  }
});
