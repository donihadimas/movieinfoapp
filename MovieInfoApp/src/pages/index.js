import Home from './home';
import Profile from './profile';
import TopRated from './toprated';
import TvShow from './TvShow';
import Login from './login';
import LoginScreen from './LoginScreen'
import Register from './register';
import Splash from './splash';
import Details from './Details';


export {Home, Profile, TopRated, TvShow, Login, Register, Splash, LoginScreen, Details};